package com.example.apiswag.main.controllers.swaggerapi;

import com.example.apiswag.main.models.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employees")
@Api(value="onlinestore", description="Operations pertaining to employee in Online BackEnd")
class EmployeeController {

    private final EmployeeRepository repository;

    EmployeeController(EmployeeRepository repository) {
        this.repository = repository;
    }


    // Aggregate root
    // tag::get-aggregate-root[]
    @ApiOperation(value = "View a list of employees",response = Employee.class)
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json")
    List<Employee> all() {
        return repository.findAll();
    }
    // end::get-aggregate-root[]

    @ApiOperation(value = "Add an employee")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = "application/json")
    Employee newEmployee(@RequestBody Employee newEmployee) {
        return repository.save(newEmployee);
    }

    // Single item
    @ApiOperation(value = "Search an employee with an ID",response = Employee.class)
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    Employee one(@PathVariable Long id) {

        return repository.findById(id)
                .orElseThrow(() -> new EmployeeNotFoundException(id));
    }

    @ApiOperation(value = "Update an employee with their ID")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json")
    Employee replaceEmployee(@RequestBody Employee newEmployee, @PathVariable Long id) {

        return repository.findById(id)
                .map(employee -> {
                    employee.setName(newEmployee.getName());
                    employee.setRole(newEmployee.getRole());
                    return repository.save(employee);
                })
                .orElseGet(() -> {
                    newEmployee.setId(id);
                    return repository.save(newEmployee);
                });
    }

    @ApiOperation(value = "Delete an employee with their ID")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "application/json")
    void deleteEmployee(@PathVariable Long id) {
        repository.deleteById(id);
    }
}