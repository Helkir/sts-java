package com.example.apiswag.main.controllers.swaggerapi;

import com.example.apiswag.main.models.*;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@RestController
class ApiController {

    @GetMapping("/apicontroller")
    public ApiModelResponse apicontroller(@RequestParam(value = "name", defaultValue = "World") String name) {
        return new ApiModelResponse(name,name+name,name+name+name); //IMPLICITEMENT DU JSON
    }

}