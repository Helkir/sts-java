package com.example.apiswag.main.controllers;

import com.example.apiswag.main.models.movie.Movie;
import com.example.apiswag.main.models.user.UserResult;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

@Controller
public class MovieController {

    @GetMapping("/")
    public String Home() {
        return "home";
    }

    @GetMapping("/movie")
    public String Movie(Model model) {
        UserResult u = getUser();
        Movie m = getMovie();

        model.addAttribute("first", u.getResult().get(0).getName().getFirst());
        model.addAttribute("large", u.getResult().get(0).getPicture().getLarge());
        model.addAttribute("title", u.getResult().get(0).getName().getTitle());
        model.addAttribute("last", u.getResult().get(0).getName().getLast());
        model.addAttribute("number", u.getResult().get(0).getLocation().getStreet().getNumber());
        model.addAttribute("name", u.getResult().get(0).getLocation().getStreet().getName());
        model.addAttribute("postcode", u.getResult().get(0).getLocation().getPostcode());
        model.addAttribute("city", u.getResult().get(0).getLocation().getCity());
        model.addAttribute("email", u.getResult().get(0).getEmail());
        model.addAttribute("phone", u.getResult().get(0).getPhone());

        model.addAttribute("movie_title", m.getTitle());
        model.addAttribute("movie_name", m.getGenres().get(0).getName());
        model.addAttribute("movie_budget", m.getBudget().equals("0") ? "unknown" : m.getBudget());
        model.addAttribute("movie_revenue", m.getRevenue().equals("0") ? "unknown" : m.getRevenue());
        model.addAttribute("movie_overview", m.getOverview());
        model.addAttribute("release_date", m.getRelease_date());
        model.addAttribute("movie_poster_path", m.getPoster_path());
        model.addAttribute("movie_backdrop_path", m.getBackdrop_path());
        model.addAttribute("movie_vote_average", Double.parseDouble(m.getVote_average()));

        return "index";
    }

    private UserResult getUser() {
        RestTemplate restTemplate = new RestTemplate();
        UserResult user = restTemplate.getForObject(
                "https://randomuser.me/api/", UserResult.class);

        return user;
    }

    private Movie getMovie() {
        Movie movie;
        int r = (int) ((Math.random() * (5000 - 100)) + 100);
        System.out.println(r);
        try {
            RestTemplate restTemplate = new RestTemplate();
            movie = restTemplate.getForObject(
                    "https://api.themoviedb.org/3/movie/" + r + "?api_key=c1eaad3f94e03f6a5f72f062564a57f6", Movie.class);
        } catch (Exception e) {
            movie = getMovie();
        }
        return movie;
    }

    @ResponseBody
    @RequestMapping("/json")
    public String data() {
        Movie m = getMovie();
        UserResult u = getUser();
        return u.toString() + "<br/><br/>" + m.toString();
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }
}
