package com.example.apiswag.main.models.user;

public class UserPicture {
    private String large;

    public String getLarge() {
        return large;
    }

    public void setLarge(String large) {
        this.large = large;
    }

    @Override
    public String toString() {
        return "{" +
                "large='" + large + '\'' +
                '}';
    }
}
