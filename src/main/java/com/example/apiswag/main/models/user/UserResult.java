package com.example.apiswag.main.models.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserResult {

    @JsonProperty("results")
    private List<UserInfo> results;

    public List<UserInfo> getResult() {
        return results;
    }

    public void setResult(List<UserInfo> result) {
        this.results = result;
    }

    @Override
    public String toString() {
        return "{" +
                "results=" + results +
                '}';
    }
}
