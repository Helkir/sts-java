package com.example.apiswag.main.models.user;

public class UserLocation {
    private UserStreet street;
    private String city;
    private String postcode;

    public UserStreet getStreet() {
        return street;
    }

    public void setStreet(UserStreet street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    @Override
    public String toString() {
        return "{" +
                "street=" + street +
                ", city='" + city + '\'' +
                ", postcode=" + postcode +
                '}';
    }
}
