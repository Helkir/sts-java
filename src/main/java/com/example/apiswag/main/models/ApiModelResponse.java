package com.example.apiswag.main.models;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class ApiModelResponse {

    private @Id @GeneratedValue Long id; // mandatory
    private String firstThing;
    private String secondThing;
    private String thirdThing;

    public ApiModelResponse(String firstThing, String secondThing, String thirdThing) {

        this.firstThing = firstThing;
        this.secondThing = secondThing;
        this.thirdThing = thirdThing;
    }

    public ApiModelResponse() { // forced by orm ...

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstThing() {
        return firstThing;
    }

    public void setFirstThing(String firstThing) {
        this.firstThing = firstThing;
    }

    public String getSecondThing() {
        return secondThing;
    }

    public void setSecondThing(String secondThing) {
        this.secondThing = secondThing;
    }

    public String getThirdThing() {
        return thirdThing;
    }

    public void setThirdThing(String thirdThing) {
        this.thirdThing = thirdThing;
    }
}