package com.example.apiswag;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiswagApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiswagApplication.class, args);
    }

}
