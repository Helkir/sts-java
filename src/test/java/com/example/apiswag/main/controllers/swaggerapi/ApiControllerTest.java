package com.example.apiswag.main.controllers.swaggerapi;

import com.example.apiswag.ApiswagApplication;
import com.example.apiswag.main.controllers.swaggerapi.ApiController;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApiswagApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("fixedport")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ApiControllerTest {

    private MockMvc mockMvcClass;
    private final static int EXPECTED_PORT = 5432;

    @BeforeAll
    public void initApiController() {
        this.mockMvcClass = MockMvcBuilders.standaloneSetup(new ApiController()).build();
    }

    @Value("${server.port}")
    private int serverPort;

    @Test
    public void givenFixedPortAsServerPort_whenReadServerPort_thenGetThePort() {
        assertEquals(EXPECTED_PORT, serverPort);
    }
    @Test
    public void testQueryWithName_JavaWeek_parameter() throws Exception {
        String argString = "javaWeek";
        this.mockMvcClass.perform(get("http://127.0.0.1:"+ serverPort +"apicontroller/?name="+argString)).andExpect(status().isOk())
                .andExpect(content().string("{\"id\":null,\"firstThing\":\""+argString+"\",\"secondThing\":\""+argString+argString+"\",\"thirdThing\":\""+argString+argString+argString+"\"}"));
    }
}